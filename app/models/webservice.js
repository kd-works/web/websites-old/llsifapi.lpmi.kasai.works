'use strict';

const snekfetch = require('snekfetch');
const data = require('../config/llsif.json');

module.exports = {
  getData: (id, pass) => {
  	if (!data[id]) return false;
  	if (data[id].pass === pass) return data[id];
  }
}
