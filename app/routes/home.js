'use strict';

const routes = require('express').Router();
const model = require('../models/webservice');

routes.get('/', (req, res, next) => res.render('index', { authenticated: req.session.authenticated, sess: req.session }));
routes.get('/login', (req, res, next) => res.render('login', { error: req.flash('error'), sess: req.session }));
routes.get('/logout', (req, res, next) => {
	delete req.session.authenticated;
	delete req.session.user_id;
	delete req.session.user_pwd;
	res.redirect('/');
});
routes.get('/account', (req, res, next) => {
  if (!req.session.authenticated) return res.redirect('/');
  let data = model.getData(req.session.user_id, req.session.user_pwd);
  res.render('account', {
    sess: req.session,
    data: data
  });
});

routes.post('/login', (req, res, next) => {
  let data = model.getData(req.body.id, req.body.password);
  if (!data) {
    req.flash('error', 'Erreur de saisie du login ou du mot de passe.');
    res.redirect('/login');
  } else {
    req.session.authenticated = true;
    req.session.user_id = req.body.id;
    req.session.user_pwd = req.body.password;
    res.redirect('/account');
  }
});

module.exports = routes;
