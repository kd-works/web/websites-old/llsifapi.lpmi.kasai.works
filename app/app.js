'use strict';

const fs = require('fs');
const path = require('path');
const express = require('express');
const compression = require('compression');
const morgan = require('morgan');
const uuid = require('node-uuid');
const flash = require('connect-flash');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const session = require('express-session')

const config = require('./config/config.json');
const port = process.env.PORT || config.port;
const app = express();

morgan.token('id', function getId(req) { return req.id; });

app.set('trust proxy', 1);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.enable('view cache');

app.use(assignId);
app.use(morgan(':id :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version :status :user-agent :res[content-length]', {
  stream: fs.createWriteStream(path.join(__dirname, 'logs/access.log'), { flags: 'a' })
}));
app.use(compression());
app.use(helmet());
app.use(flash());
app.use(cookieParser());
app.use(session({
  secret: 'secret',
  name: 'name',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 30 * 24 * 60 * 60 * 1000
  }
}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/', require('./routes/home'));

app.use((req, res, next) => res.status(404).render('404'));
app.use((req, res, next) => res.status(500).render('500'));

function assignId(req, res, next) {
  req.id = uuid.v4();
  next();
}

app.listen(port, () => console.log(`${config.name} started on port ${port}`));
